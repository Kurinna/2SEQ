# 2Seq

All scripts of 2Seq can be used on any organism with X/Y chromosomes.

1. Installation
2. Running the Scripts
  + 2.1 Motif Search
  + 2.2 Pairing
  + 2.3 TSS assignment
  + 2.4 Peak assignment


## 1. Installation
Just copy all files into your working directory. You will need to have the following tools installed:

- R
- Python
- Biopython

For all scripts to generate their plots, you will also need some R packages:

- ggplot2
- VennDiagram


## 2. Running the Scripts
### 2.1 Motif Search
#### Usage
The python script `motifSearch.py` is used to search for all motifs in a fasta file and outputs their posititons with additional info.

python motifSearch.py<font color="green"> motifs.fa </font><font color="blue"> GenomeDirectory </font><font color="red"> Label </font>

Replace <font color="green">motifs.fa.py</font> with the fasta file containing all your motifs sequences, <font color="blue">GenomeDirectory</font> with the path to your genome and <font color="purple">SearchLabel</font> with a label for your output file.


#### Input

##### Motifs
Motifs.fa contains the IUPAC coded sequences to search for in standard fasta format.

##### Chromosome sequences
Chromosome sequences are stored in fasta files named according to their respective chromosome.  
Head of chr1.fa file:

```
>chr1
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
```

#### Output
All positions of each sequence in the input fasta file get stored in a comma separated format as Label_motifPositions.csv

|variable|description|
|:----|:----------------------------------------|
|chromosome|chromosome, on which a certain motif was found|
|sequence|actual sequence that matched the motif|
|start|start of the motif|
|end|end of the motif|
|motif|specifies, what motif was matched|
|strand|1 for forward strand, 2 for reverse strand|

### 2.2 Pairing
`pairing.R` pairs the motif with the lower hit counts (blue) with the closest hit of the other motif (orange).
![alt text](pairing.PNG)

#### Usage

Rscript pairing.R<font color="green"> motifs.fa </font><font color="blue"> MotifPositions.csv </font><font color="red"> Label </font><font color="purple">#ofSamples</font>

<font color="blue"> MotifPositions.csv </font> sould be the path to the output file of `motifSearch.py`. <font color="purple">#ofSamples</font> determines, if real hits should be paired (set by `0`) or artificial random distributions of the two motifs should be generated and paired (any number `>0`). 

#### Output

Label_MotifPairDistances.csv contains the paired motifs, their orientation (strand) and actual sequence

|variable|description|
|:----|:----------------------------------------|
|chromosome|chromosome, on which the pair is positioned|
|firstMotif|specifies, which motif comes first on the forward strand (1 == first motif, 2 == second motif)|
|start1|start of the first motif|
|end1|end of the first motif|
|start2|start of the second motif|
|end2|end of the second motif|
|distance|distance between |
|strand1|strand, on which the first motif was found|
|strand2|strand, on which the second motif was found|
|sample|specifies random sampling run or actual sequence for the real DNA search|

#### Plotting

Plots of motif distances are created by `pairingPlots.R` to let it up to the user to use them or create own graphs.

Rscript pairingPlots.R<font color="green"> Label_MotifPairDistances.csv </font><font color="blue"> Label_RandomPairDistances.csv </font><font color="red"> Label </font>

This script uses the unmodified output files of the pairing.R script. It produces 8 different graphs:

|Filename|information|
|:----|:----------------------------------------|
|Label_M1-M2_distances_dens147.png|Density curves of all pairs with a distance lower than 147bp but without overlaps|
|Label_M1-M2_distances_hist147.png|Histogram representation of all pairs with a distance lower than 147bp but without overlaps|
|Label_M1-M2_distances_hist147noRandom.png|Histogram representation of only real pairs with a distance within a nucleosome without overlaps|
|Label_M1-M2_distances_hist147noRandomOverlap.png|Histogram representation of only real pairs with a distance within a nucleosome|
|Label_M1-M2_distances_hist2000.png|Histogram representation of all pairs with a distance between 0 and 2000|
|Label_M1-M2_distances_hist5000.png|Histogram representation of all pairs with a distance between 0 and 5000|
|Label_M1-M2_distances_RandomVsExperimentalDensity.png|Density representation of all pairs with a distance between 0 and 20'000|
|Label_M1-M2_distances_RandomVsExperimentalHistogram.png|Histogram representation of all pairs with a distance between 0 and 20'000|

The x-axis range can easily be adapted to your needs with the `scale_x_continuous(limits=c(0,20000))` option

### 2.3 TSS assignment

You can use `TSS-Pair-distance.R` to assign the closest pair to each TSS in the same way as the motif-motif assignments. It directly uses the output of pairing.R 

Rscript TSS-Pair-distance.R<font color="green"> TSS.bed </font><font color="blue"> Label </font><font color="red"> -random RandomPairs.csv / MotifPairs.csv </font>



##### Input
The file containing the TSS information has to follow the following structure. The values are tab separated.

|variable|description|
|:----|:----------------------------------------|
|id|identifier of the TSS|
|chromosome|chromosome on which the gene is found|
|position|start of transcription (TSS)|
|strand|strand on which the gene is found|
|name|name of the gene|

Pairs need to be in the same structure as obtained from the pairing.R script.

##### Output

|variable|description|
|:----|:----------------------------------------|
|chr|chromosome|
|start|start of pair|
|end|end of pair|
|tss|tss position|
|distance|distance between the tss and the closer end of the pair-range|
|sample|specifies random sampling run or actual sequence for the real DNA search|
|gene_id|ensembl gene identifier|

#### Plotting

Plots of motif distances are created by `TSS-distances-plot.R` to let it up to the user to use them or create own graphs.

Rscript TSS-distances-plot.R<font color="green"> MotifPairTSS.csv </font><font color="blue"> RandomPairTSS.csv </font><font color="red"> Label </font>

#### Input

The only input needed for plotting are the output files of the TSS-Pair-distnace.R script for real and random motif pairs.

#### Output

This script generates two plots. <font color="red"> Label</font>_density.png shows the density function of the distance between all TSS and their closest pair. If the pair is found downstream of the TSS, it is counted as positive distance, whereas pairs upstream of the TSS are counted as positive distance.
<font color="red"> Label</font>_absoluteDensity.png shows the same distances independent of their orientation relative to the gene.

### 2.4 Peak assignment

To identify putative target pairs, the pairs can be overlayed with any kind of range information such as Chip-Seqs.

Python Pair-Peak-Combinator.py<font color="green"> Peaks.bed </font><font color="blue"> PairPositions.csv </font><font color="red"> Label </font>

#### Input

The peaks need to follow standard bed format:

|variable|description|
|:----|:----------------------------------------|
|chr|chromosome|
|start|start of peak|
|end|end of peak|

The pair positions need to follow the pairing.R script output.

#### Output

|file name|information|
|:----|:----------------------------------------|
|Label.csv|pairs where both motifs are fully below a peak|
|Label_onlyM1.csv|pairs where only motif1 is fully below a peak|
|Label_onlyM2.csv|pairs where only motif2 is fully below a peak|

The structures of these three files are the same:

|variable|description|
|:----|:----------------------------------------|
|chromosome|chromosome, on which the pair is positioned|
|firstMotif|specifies, which motif comes first on the forward strand (1 == first motif, 2 == second motif)|
|start1|start of the first motif|
|end1|end of the first motif|
|start2|start of the second motif|
|end2|end of the second motif|
|distance|distance between |
|strand1|strand, on which the first motif was found|
|strand2|strand, on which the second motif was found|
|sequence / run|actual sequence found; for random sampling: # of the run|
|peak-start|start of the peak|
|peak-end|end of the peak|

#### Plotting

An R script is used to create Venn diagrams that show the relations between pairs and peaks.

Rscript VennDiagrams.R<font color="green"> PairPositions.csv </font><font color="blue"> Peaks.bed </font><font color="red"> Label </font>

##### Input

The pair positions have to be in the same format as retrieved from pairing.R and the peaks need to be in standard bed format.

##### Output

The output is saved in a pdf (either ending in "M1M2" for real pairs or "Random" for random samplings) to keep the Venn diagrams clean and ordered in case multiple pairs and peaks are used. Each pdf contains first the venn diagram showing pairs with only one of the two motifs below peaks (blue: M1, pink: M2) and second the venn diagram showing the number of pairs (blue) having both motifs below peaks (pink).


