import sys
class Range:
	def __init__(self,start,end):
		self.start = start
		self.end = end

	def __str__(self):
		return 'Start={0}, End={1}'

class Pair:
	def __init__(self,start1,end1,start2,end2,firstM,label1,label2,motSeq):
		self.start1 = start1
		self.end1 = end1
		self.start2 = start2
		self.end2 = end2
		self.firstM = firstM
		self.label1 = label1
		self.label2 = label2
		self.motSeq = motSeq

	def __str__(self):
		return 'Start1={0}, End1={1}, Start2={2}, End2={3}, First Motif={4}, Label1={5}, Label2={6} Motif Sequence={7}'

class Hit:
	def __init__(self,Mstart,Mend,Pstart,Pend):
		self.Mstart = Mstart
		self.Mend = Mend
		self.Pstart = Pstart
		self.Pend = Pend
	def __str__(self):
		return 'Motif range:{0},{1}; Peak range:{2},{3}'

def readBED(File):
	validChrom = ['chr'+str(i) for i in range(1,23)] + ['chrX', 'chrY']
	BEDInfo = {}
	with open(File) as f:
		line = f.readlines()
		for l in line:
			elements = l.split("\t")
			if elements[0] != "chr":
				chrom = elements[0]
				start = int(elements[1])
				end = int(elements[2])
				if chrom in validChrom:
					if chrom not in BEDInfo.keys():
						BEDInfo[chrom] = [Range(start,end)]
					else:
						BEDInfo[chrom].append(Range(start,end))

	return(BEDInfo)

def readPairs(File):
	Chrom = range(1,25)
	validChrom = ['chr'+str(i) for i in range(1,23)] + ['chrX', 'chrY']
	PairInfo = {}
	with open(File) as f:
		line = f.readlines()
		for l in line:
			elements = l.split(",")
			c = elements[0]
			if c in validChrom:
				chrom = c
			else:
				if c == "23":
					chrom = "chrX"
				elif c == "24":
					chrom = "chrY"
				else:
					chrom = "chr"+c
			start1 = int(elements[2])
			end1 = int(elements[3])
			start2 = int(elements[4])
			end2 = int(elements[5])
			label1 = int(elements[7])
			label2 = int(elements[8])
			firstM = int(elements[1])
			motSeq = elements[9].split("\n")[0]
			if chrom in validChrom:
				if chrom not in PairInfo.keys():
					PairInfo[chrom] = [Pair(start1,end1,start2,end2,firstM,label1,label2,motSeq)]
				else:
					PairInfo[chrom].append(Pair(start1,end1,start2,end2,firstM,label1,label2,motSeq))
	return(PairInfo)

def main():
	## load BED files for peaks and Motifs
	# 1: peaks as bed file
	# 2: Pairs in format: chr,firstMotif,start1,end1,start2,end2,label1,label2,sequence
	# 3: savefile label
	print("Reading of BED files ...")
	print("------- Settings -------")
	peakFile = sys.argv[1]
	peaksChr = readBED(peakFile)
	print("> Peaks: "+peakFile)

	MotifFile = sys.argv[2]
	MotifChr = readPairs(MotifFile)
	print("> Motifs: "+MotifFile)

	## Output file to save hits in
	HitFile = sys.argv[3]+".csv"
	Hits = open(HitFile,"w")

	HitFile1 = sys.argv[3]+"_onlyM1"+".csv"
	HitFile2 = sys.argv[3]+"_onlyM2"+".csv"
	Hits1 = open(HitFile1,"w")
	Hits2 = open(HitFile2,"w")

	print("> Output-Files:")
	print(">> both motifs under peak: "+HitFile)
	print(">> only M1s under peak:	"+HitFile1)
	print(">> only M2s under peak:	"+HitFile2)

	## go through each Chromosome
	print("\n")
	print("------- Analysis -------")
	validChrom = ['chr'+str(i) for i in range(1,23)] + ['chrX', 'chrY']
	for c in validChrom:
		print("Analysis of "+c)
		if c in peaksChr.keys():
			peakSet = peaksChr[c]
		else:
			print("# no peaks -> skip")
			peakSet = []
		if c in MotifChr.keys():
			MotifSet = MotifChr[c]
		else:
			print("# no motifs -> skip")
			MotifSet = []
		counter = 0
		cn = c.split("chr")[1]
		print("# of peaks:\t"+str(len(peakSet)))
		print("# of motifs:\t"+str(len(MotifSet)))
		for r1 in MotifSet:
			for r2 in peakSet:
				# both motifs fully below peak
				if (r2.start <= r1.start1 <= r2.end) and (r2.start <= r1.end1 <=r2.end) and (r2.start<= r1.start2 <=r2.end) and (r2.start <= r1.end2 <= r2.end):
					counter += 1
					Hits.write(str(cn)+","+str(r1.firstM)+","+str(r1.start1)+","+str(r1.end1)+","+str(r1.start2)+","+str(r1.end2)+","+str(r1.start2-r1.end1-1)+","+str(r1.label1)+","+str(r1.label2)+","+r1.motSeq+","+str(r2.start)+","+str(r2.end)+"\n")
				# only the first motif of pair fully below peak
				elif (r2.start <= r1.start1 <= r2.end) and (r2.start <= r1.end1 <= r2.end) and (r1.start2 < r2.start or r1.start2 > r2.end or r1.end2 < r2.start or r1.end2 > r2.end):
					if r1.firstM == 1: #first position == RE
						Hits1.write(str(cn)+","+str(r1.firstM)+","+str(r1.start1)+","+str(r1.end1)+","+str(r1.start2)+","+str(r1.end2)+","+str(r1.start2-r1.end1-1)+","+str(r1.label1)+","+str(r1.label2)+","+r1.motSeq+","+str(r2.start)+","+str(r2.end)+"\n")
					elif r1.firstM == 2: #first position == ARE
						Hits2.write(str(cn)+","+str(r1.firstM)+","+str(r1.start1)+","+str(r1.end1)+","+str(r1.start2)+","+str(r1.end2)+","+str(r1.start2-r1.end1-1)+","+str(r1.label1)+","+str(r1.label2)+","+r1.motSeq+","+str(r2.start)+","+str(r2.end)+"\n")
				# only the second motif of pair fully below peak
				elif (r2.start <= r1.start2 <= r2.end) and (r2.start <= r1.end2 <= r2.end) and (r1.start1 < r2.start or r1.start1 > r2.end or r1.end1 < r2.start or r1.end1 > r2.end):
					if r1.firstM == 1: #first position == RE
						Hits2.write(str(cn)+","+str(r1.firstM)+","+str(r1.start1)+","+str(r1.end1)+","+str(r1.start2)+","+str(r1.end2)+","+str(r1.start2-r1.end1-1)+","+str(r1.label1)+","+str(r1.label2)+","+r1.motSeq+","+str(r2.start)+","+str(r2.end)+"\n")
					elif r1.firstM == 2: #first position == ARE
						Hits1.write(str(cn)+","+str(r1.firstM)+","+str(r1.start1)+","+str(r1.end1)+","+str(r1.start2)+","+str(r1.end2)+","+str(r1.start2-r1.end1-1)+","+str(r1.label1)+","+str(r1.label2)+","+r1.motSeq+","+str(r2.start)+","+str(r2.end)+"\n")
		print("# of hits:\t"+str(counter))

	Hits.close()
	Hits1.close()
	Hits2.close()

main()
