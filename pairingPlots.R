rm(list=ls())
# call this file with
#   6;  Motif-pairs     (csv)
#   7;  Random-pairs    (csv)
#   8;  label

library(ggplot2)

showkb <- function(x){
  return(x/1000)
}

print("Reading of arguments ...",quote=F)
args <- commandArgs()

print("Reading of input files ...",quote=F)
pairs <-read.csv(args[6],header=F,stringsAsFactors=F)
names(pairs) <- c("chromosome","firstMotif","start1","end1","start2","end2","distance","strand1","strand2","sequence")
random <- read.csv(args[7],header=F,stringsAsFactors=F)
names(random) <- c("chromosome","firstMotif","start1","end1","start2","end2","distance","strand1","strand2","run")


randomD <- data.frame(random$distance,paste("random ",random$run,sep=""))
experimentalD <- data.frame(pairs$distance,c("experimental"))
names(randomD) <- c("distance","origin")
names(experimentalD) <- c("distance","origin")

Distances <- rbind(experimentalD,randomD)
Distances$distance <- round(Distances$distance)

plot <- ggplot(experimentalD,aes(distance))
plot +  geom_density(aes(y=..count..)) + geom_histogram(binwidth=1) +
  labs(x="distance between two paired motifs", y="density function") +
  theme_bw() + theme(legend.title=element_blank(), panel.grid.major=element_blank(), panel.grid.minor=element_blank(), axis.line=element_line(colour="black"), axis.text=element_text(size=15,colour="black"), text=element_text(size=15)) +
  scale_x_continuous(limits=c(-14,147))
ggsave(paste(args[8],"M1-M2_distances_hist147noRandomOverlap.png",sep="_"))

plot <- ggplot(experimentalD,aes(distance))
plot +  geom_density(aes(y=..count..)) + geom_histogram(binwidth=1) +
  labs(x="distance between two paired motifs", y="density function") +
  theme_bw() + theme(legend.title=element_blank(), panel.grid.major=element_blank(), panel.grid.minor=element_blank(), axis.line=element_line(colour="black"), axis.text=element_text(size=15,colour="black"), text=element_text(size=15)) +
  scale_x_continuous(limits=c(0,147))
ggsave(paste(args[8],"M1-M2_distances_hist147noRandom.png",sep="_"))

plot <- ggplot(Distances,aes(distance,colour=origin,linetype=origin))
plot +  geom_density() + 
  labs(x="distance between two paired motifs", y="density function") +
  theme_bw() + theme(legend.title=element_blank(), panel.grid.major=element_blank(), panel.grid.minor=element_blank(), axis.line=element_line(colour="black"), axis.text=element_text(size=15,colour="black"), text=element_text(size=15)) +
  scale_x_continuous(limits=c(0,147)) + scale_y_continuous(label=scales::scientific_format())
ggsave(paste(args[8],"M1-M2_distances_dens147.png",sep="_"))

plot <- ggplot(Distances,aes(distance,fill=origin))
plot +  geom_histogram(position="identity",binwidth=1, alpha=1) + 
  labs(x="distance between two paired motifs", y="number of occurences") +
  theme_bw() + theme(legend.title=element_blank(), panel.grid.major=element_blank(), panel.grid.minor=element_blank(), axis.line=element_line(colour="black"), axis.text=element_text(size=15,colour="black"), text=element_text(size=15)) +
  scale_x_continuous(limits=c(0,147)) + scale_y_continuous(limits=c(0,600))
ggsave(paste(args[8],"M1-M2_distances_hist147.png",sep="_"))

plot <- ggplot(Distances,aes(distance,fill=origin))
plot +  geom_histogram(position="identity",binwidth=10, alpha=1) + 
  labs(x="distance between two paired motifs, kb", y="number of occurences") +
  theme_bw() + theme(legend.title=element_blank(), panel.grid.major=element_blank(), panel.grid.minor=element_blank(), axis.line=element_line(colour="black"), axis.text=element_text(size=15,colour="black"), text=element_text(size=15)) +
  scale_x_continuous(limits=c(0,2000),label=showkb)
ggsave(paste(args[8],"M1-M2_distances_hist2000.png",sep="_"))

plot <- ggplot(Distances,aes(distance,fill=origin))
plot +  geom_histogram(position="identity",binwidth=10, alpha=1) +
  labs(x="distance between two paired motifs, kb", y="number of occurences") +
  theme_bw() + theme(legend.title=element_blank(), panel.grid.major=element_blank(), panel.grid.minor=element_blank(), axis.line=element_line(colour="black"), axis.text=element_text(size=15,colour="black"), text=element_text(size=15)) +
  scale_x_continuous(limits=c(0,5000),label=showkb)
ggsave(paste(args[8],"M1-M2_distances_hist5000.png",sep="_"))

plot <- ggplot(Distances,aes(distance,fill=origin))
plot +  geom_histogram(position="identity",bins=2000, alpha=1) +
  labs(x="distance between two paired motifs, kb", y="number of occurences") +
  theme_bw() + theme(legend.title=element_blank(), panel.grid.major=element_blank(), panel.grid.minor=element_blank(), axis.line=element_line(colour="black"), axis.text=element_text(size=15,colour="black"), text=element_text(size=15)) +
  scale_x_continuous(limits=c(0,20000),label=showkb)
ggsave(paste(args[8],"M1-M2_distances_RandomVsExperimentalHistogram.png",sep="_"))

plot <- ggplot(Distances,aes(distance,fill=origin))
plot +  geom_density(position="identity", alpha=0.2) +
  labs(x="distance between two paired motifs, kb", y="density function of distance between two motifs") +
  theme_bw() + theme(legend.title=element_blank(), panel.grid.major=element_blank(), panel.grid.minor=element_blank(), axis.line=element_line(colour="black"), axis.text=element_text(size=15,colour="black"), text=element_text(size=15)) +
  scale_x_continuous(limits=c(0,20000),label=showkb)
ggsave(paste(args[8],"M1-M2_distances_RandomVsExperimentalDensity.png",sep="_"))

