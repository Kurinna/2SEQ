
from Bio.Data import IUPACData
from Bio.Seq import Seq
from Bio import SeqIO
import os
import sys
import regex as re
from itertools import product

#type class for Genes containing specific gene information
class Gene:
	def __init__(self, geneName, chrom, TSS, label):
		self.geneName = geneName
		self.chrom = chrom
		self.TSS = TSS
		self.label = label

	def __str__(self):
		return 'Gene={0}, Chromosome={1}, TSS={2}, Label={3}'.format(self.geneName, self.chrom, int(self.TSS), self.label)

def iupac2re(seq):
	adv = (IUPACData.ambiguous_dna_values[x] for x in str(seq))
	return ''.join('[{0}]'.format(x) for x in adv)

class Match:
	def __init__(self, respElement, start, end, label):
		self.respElement = respElement
		self.start = start
		self.end = end
		self.label = label

	def __str__(self):
		return 'RE={0}, Start={1}, End={2}, Label={4}'.format(self.respElement, self.start, self.end, self.center, self.label)

class FileUtilities(object):
	@staticmethod
	def getseq(file):
		with open(file, 'rU') as f:
			return str(SeqIO.read(f, 'fasta').seq)

def extend_ambiguous_dna(seq):
   """return list of all possible sequences given an ambiguous DNA input"""
   d = IUPACData.ambiguous_dna_values
   r = []
   for i in product(*[d[j] for j in seq]):
      r.append("".join(i))
   return r

def getMotifHits(motif, dna):
	hitList = []

	print('Strand being searched: forward')
	i1 = re.finditer(iupac2re(motif), dna, overlapped=True)
	for match in i1:
		hitList.append(Match(motif, match.start(), match.end(), "1"))

	print('                       reverse')
	i2 = re.finditer(iupac2re(motif.reverse_complement()), dna, overlapped=True)
	for match in i2:
		hitList.append(Match(motif, match.start(), match.end(), "2"))

	return hitList

def main():
	## load files for genome and motifs
	# 1: motifs.fa
	# 2: path to genome
	# 3: savefile label

	print("** Read input files:")

	#define outputfile
	Hitfile = sys.argv[3]+"_motifPositions.csv"
	MotifHits = open(Hitfile, "w")

    #load sequences to search for and safe them in dictionary
	print('* Read motifs')
	motifFile = sys.argv[1]#"motifs.fa"
	motifs = {}
	for seq_record in SeqIO.parse(motifFile, "fasta"):
		motifs[seq_record.id] = seq_record.seq

	#read in all chromosome-datasets
	print('* Genome-generation ...')
	chrSeq = {}
	fileCounter = 0
	for subdir, dirs, files in os.walk(sys.argv[2]):
		for file in files:
			if (file.endswith('.fa')):
				sys.stdout.write('.')
				fileCounter += 1
				if (fileCounter % 5) == 0:
					sys.stdout.write('|')
				filePath = os.path.join(subdir, file)
				chrNr = filePath.replace('.fa', '').split('/')[-1]
				sequence = FileUtilities().getseq(filePath)
				chrSeq[chrNr] = sequence
	sys.stdout.write('\n')

	#search occurences of motifs in each Chromosome
	motifOccurences = {}
	for motif in motifs.keys():
		motifOccurences[motif]=0

	print("** Find motif hits")
	chrMotifDict = {}
	lowestHitCount = {}
	chrnum = len(chrSeq.keys())
	for i in ['chr'+str(i) for i in range(1,chrnum-1)] + ['chrX', 'chrY']:
		motifHits = {}
		lowestHitCount[i] = ""
		temp = 9999999999
		print('>>>>> Motif hits on '+i+' <<<<<')
		print('Length of chromosome: '+str(len(chrSeq[i])))
		for motif in motifs.keys():
			motifHits[motif] = getMotifHits(motifs[motif],chrSeq[i])
			print(motif+": "+str(len(motifHits[motif])))
			motifOccurences[motif] = motifOccurences[motif] + len(motifHits[motif])
			for hit in motifHits[motif]:
				if hit.label == "1":
					hitSequence = chrSeq[i][hit.start:hit.end]
				else:
					hitSequence = str(Seq(chrSeq[i][hit.start:hit.end]).reverse_complement())
				MotifHits.write(str(i)+","+hitSequence+","+str(hit.start+1)+","+str(hit.end)+","+str(hit.respElement)+","+str(hit.label)+"\n")

			if len(motifHits[motif]) < temp:
				temp = len(motifHits[motif])
				lowestHitCount[i] = motif

		#add dict with hits per motif to chromosome-dict
		chrMotifDict[i] = motifHits

	print(">> Total motif hits genome-wide:")
	for motif in motifOccurences.keys():
		print(motif+': '+str(motifOccurences[motif]))
	print('----------------------------------')
	print("\nIndividual hits saved in: "+Hitfile)
	MotifHits.close()

main()
