rm(list=ls())

# arguments:
#   6:  experimental data
#   7:  random data
#   8:  label

library(ggplot2)

args <- commandArgs()

EData <- read.csv(args[6],header=F,stringsAsFactors = F)
RData <- read.csv(args[7],header=F,stringsAsFactors = F)
names(EData) <- c("Chromosome","REAREstart","REAREend","TSS","Distance","run","Gene")
names(RData) <- c("Chromosome","REAREstart","REAREend","TSS","Distance","run","Gene")
EData$run <- c("experimental")
RData$run <- paste("random ",RData$run,sep="")

data <- rbind(EData,RData)
data$run <- as.factor(data$run)

dataS <- subset(data,abs(Distance)<10000)

showkb <- function(x){
  return(x/1000)
}


plot <- ggplot(dataS,aes(Distance,colour=run,linetype=run))
plot + geom_density() + labs(x="distance TSS - REARE, kb",y="density function") +
  theme_bw() + theme(legend.title=element_blank(),text=element_text(size=20,colour="black"),panel.grid.major=element_blank(), panel.grid.minor=element_blank(), axis.line=element_line(colour="black")) +
  scale_x_continuous(label=showkb) 
ggsave(paste(args[8],"_density.png",sep=""))

dataSA <- dataS
dataSA$Distance <- abs(dataS$Distance)
plot <- ggplot(dataSA,aes(Distance,colour=run,linetype=run))
plot + geom_density(position="identity") + labs(x="absolute distance TSS - REARE, kb",y="density function") +
  theme_bw() + theme(legend.title=element_blank(),text=element_text(size=20,colour="black"),panel.grid.major=element_blank(), panel.grid.minor=element_blank(), axis.line=element_line(colour="black")) +
  scale_x_continuous(label=showkb) + scale_y_continuous(label=scales::scientific_format())
ggsave(paste(args[8],"_absoluteDensity.png",sep=""))